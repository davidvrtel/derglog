package logger

// Imports
import (
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

// Exportables
var Log = GetLogger(true)

// Definitions
// formatter for prefix of the logger
type prefixWriter struct {
	f func() string
	w io.Writer
}

func (p prefixWriter) Write(b []byte) (n int, err error) {
	if n, err = p.w.Write([]byte(p.f())); err != nil {
		return
	}
	nn, err := p.w.Write(b)
	return n + nn, err
}

type LogLevel uint

const (
	Debug LogLevel = iota
	Info
	Warn
	Error
)

// Main logger struct
type Logger struct {
	Level  LogLevel
	logger *log.Logger
}

// methods for logging with each level
func (l *Logger) Debug(msg ...any) {
	if l.Level <= 0 {
		l.logger.Print("\x1b[94mDEBUG\x1b[0m ", fmt.Sprintln(msg...))
	}
}

func (l *Logger) Info(msg ...any) {
	if l.Level <= 1 {
		l.logger.Print("\x1b[0mINFO\x1b[0m  ", fmt.Sprintln(msg...))
	}
}

func (l *Logger) Warn(msg ...any) {
	if l.Level <= 2 {
		l.logger.Print("\x1b[93mWARN\x1b[0m  ", fmt.Sprintln(msg...))
	}
}

func (l *Logger) Error(msg ...any) {
	if l.Level <= 3 {
		l.logger.Print("\x1b[31mERROR\x1b[0m ", fmt.Sprintln(msg...))
	}
}

// Initialization with selectable trace of file
func GetLogger(fileTrace bool) Logger {
	var logger = log.New(os.Stderr, "", 0)
	if fileTrace {
		logger.SetFlags(log.Llongfile)
	} else {
		logger.SetFlags(0)
	}
	logger.SetOutput(prefixWriter{
		f: func() string { return time.Now().Format("2006-01-02T15:04:05.000Z0700") + " " },
		w: log.Writer(),
	})

	return Logger{
		Level:  Info,
		logger: logger,
	}

}
