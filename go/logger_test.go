package logger

import (
	"testing"
)

func TestDefaultLogger(t *testing.T) {
	Log.Level = Debug
	Log.Debug("Debug message")
	Log.Info("Info message")
	Log.Warn("Warn message")
	Log.Error("Error message")
}

func TestCustomLoggerWithTracing(t *testing.T) {
	var log = GetLogger(true)
	log.Level = Debug
	log.Debug("Debug message")
	log.Info("Info message")
	log.Warn("Warn message")
	log.Error("Error message")
}

func TestCustomLoggerWithoutTracing(t *testing.T) {
	var log = GetLogger(false)
	log.Level = Debug
	log.Debug("Debug message")
	log.Info("Info message")
	log.Warn("Warn message")
	log.Error("Error message")
}
